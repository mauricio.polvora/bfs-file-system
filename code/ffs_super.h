#define FFS_SUPER_H

/* Disk layout
   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
   | SuB | BMi | Ino | Dir | BMd | Dt1 | Dt2 | ... | END |
   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
*/

#define SB_OFFSET	0
#define BMi_OFFSET	1

#define FS_MAGIC	0xf0f03410

//Amount of potential error situations checked for the superblock
#define SB_ERR_CK 8

struct super {
  unsigned int fsmagic;
  unsigned int nblocks;
  unsigned int nbmapblocksinodes;	// Always 1 in BFS
  unsigned int ninodeblocks;
  unsigned int ninodes;
  unsigned int nbmapblocksdata;		// Always 1 in BFS
  unsigned int ndatablocks;
  unsigned int startInArea;
  unsigned int startDtBmap;
  unsigned int startDtArea;
  unsigned int mounted;
};

union sb_block {
  struct super sb;
  unsigned char data[DISK_BLOCK_SIZE];
};


/* operations on superblock structures

  read: reads in a superblock from disk
    parameters:
     @out: pointer to superblock structure

  print: prints a superblock with minimum information
    parameters:
     @in: pointer to superblock structure

  debug: prints a superblock with extra information
    parameters:
     @in: pointer to superblock structure

  check: checks for potential errors in the data structure lead on by corruption or crash.
    parameters:
      @in[0]: pointer to superblock structure
      @in[1]: pointer to error flag

      @out: when 0 signals the correct functioning of the program
            when <0 signals an error specified by the accompanying ffs_errno.h file  

*/


struct super_operations {
  int (*read)(struct super *sb);
  void (*print)(const struct super *sb);
  void (*debug)(const struct super *sb);
  int (*check)(struct super *sb, unsigned int *erflag);
};
