#include <stdio.h>
#include <string.h>
#include <math.h>

#ifndef FFS_INODE_H
#include "ffs_inode.h"
#endif

#ifndef FFS_BYTEMAP_H
#include "ffs_bytemap.h"
#endif

static void inode_print(unsigned int number, struct inode *in)
{

  int max_data;
  printf("%d:\n", number);
  printf("\t%s\n", (in->isvalid) ? "valid" : "invalid");
  if (in->isvalid)
  {
    printf("\t%u\n", in->size);
    max_data = ceil((float)in->size / DISK_BLOCK_SIZE);

    for (int i = 0; i < max_data; i++)
    {
      printf("\t\t%u\n", in->direct[i]);
    }

    for (int i = max_data; i < POINTERS_PER_INODE; i++)
    {
      printf("\t\t%s\n", "NULL");
    }
  }
}

static int inode_printTable(unsigned int ninodeblocks, unsigned int ninodes,
                            unsigned int inodesStartBlock)
{
  int ercode;
  union in_block in_b;
  int left = ninodes;
  int inode_number = 0;

  printf("i-nodes:\n");

  for (int inoblk = 0; inoblk < ninodeblocks; inoblk++)
  {
    ercode = disk_ops.read((inoblk + inodesStartBlock), in_b.data);
    if (ercode < 0)
      return ercode;

    int entry = 0;
    while ((left) && (entry < INODES_PER_BLOCK))
    {
      inode_print(inode_number, &in_b.ino[entry]);
      left--;
      entry++;
      inode_number++;
    }
    //if ( entry%INODES_PER_BLOCK ) printf("\n"); // last NL for general case
  }

  return 0;
}

/* inode (global) number is decomposed into inode block number
   and offset within that block. The inode block number starts at 0 */

static void inode_location(unsigned int numinode,
                           unsigned int *numblock, unsigned int *offset)
{
  *numblock = numinode / INODES_PER_BLOCK;
  *offset = numinode % INODES_PER_BLOCK;
}

// read an i-node from disk
static int inode_read(unsigned int startInArea, unsigned int absinode, struct inode *in)
{
  int ercode;
  unsigned int block, offset;
  union in_block in_b;

  inode_location(absinode, &block, &offset);
  // read the inode block
  ercode = disk_ops.read((block + startInArea), in_b.data);
  if (ercode < 0)
    return ercode;

  // extract the inode information from the block into inode *in
  *in = in_b.ino[offset];

  return 0;
}

static void inode_clear(struct inode *in)
{

  memset(&in, 0, sizeof(in));
}

// For option A prints a buffer of data up to toPrint chars

void f_data_print(unsigned char *buf, int toPrint)
{
  int left = toPrint, entry = 0;

  // prints 16 entries per line
  while (left)
  {
    if ((entry + 1) % 16)
      printf("%c ", buf[entry]);
    else
      printf("%c\n", buf[entry]);
    left--;
    entry++;
  }
  if (entry % 16)
    printf("\n"); // last NL for general case
}

static int inode_printFileData(unsigned int startInArea, unsigned int absinode,
                               unsigned int startDtArea)
{
  int ercode, size, toPrint, max_data;
  unsigned int block, offset;
  union in_block in_b;
  unsigned char buf[DISK_BLOCK_SIZE];

  inode_location(absinode, &block, &offset);
  // read the data block containing the inode number absinode
  ercode = disk_ops.read((block + startInArea), in_b.data);
  if (ercode < 0)
    return ercode;

  if (!in_b.ino[offset].isvalid)
    return 0;

  printf("\nPrinting contents of file(inode) %d\n", absinode);

  size = in_b.ino[offset].size;
  if (!size)
  {
    printf("** NO DATA **\n");
    return 0;
  }
  // print the contents of the data blocks
  max_data = ceil((float)in_b.ino[offset].size / DISK_BLOCK_SIZE);

  for (int i = 0; i < max_data; i++)
  {
    if (i == max_data - 1)
      toPrint = size - ((max_data - 1) * 512);
    else
      toPrint = DISK_BLOCK_SIZE;
    disk_ops.read(startDtArea + in_b.ino[offset].direct[i], buf);
    f_data_print(buf, toPrint);
  }

  return 0;
}

int inode_check(struct super *sb, struct bytemap *in_bmap, struct bytemap *dt_bmap, unsigned int *erflag)
{
  int ercode;
  struct inode in;

  for (int i = 0; i < sb->ninodes; i++)
  {

    //Inodes are read consecutively
    ercode = inode_read(sb->startInArea, i, &in);
    if (ercode < 0)
      return ercode;

    //If the inode is currently allocated, then it is marked as such in the provided 'bmap'
    if (in.isvalid)
      in_bmap->bmap[i] = 1;

    int pointer;

    int max_data = ceil((float)in.size / DISK_BLOCK_SIZE);

    for (int j = 0; j < max_data; j++)
    {
      pointer = in.direct[j];

      //Unfixable error, there are two inodes which point to a single block. (and without an access schedule, it is impossible to distinguish which has last modified the block)
      if (dt_bmap->bmap[pointer] == 1)
      {
        (*erflag)++;
      }
      else
      {
        dt_bmap->bmap[pointer] = 1;
      }
    }

  }

  return 0;
}

struct inode_operations inode_ops = {
    .clear = inode_clear,
    .read = inode_read,
    .printFileData = inode_printFileData,
    .printTable = inode_printTable,
    .check = inode_check};
