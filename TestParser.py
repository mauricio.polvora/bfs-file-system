from os import system, listdir

# Try to compile code
if system("cd code ; make dumpBFS")==0:
	print("Files successfully compiled.")
else:
	print("Files failed to compile.")
	exit(0)

# Run the tests
for i in range(1, 9):
	i = str(i)
	command = "cd code/; ./dumpBFS ../data/" +i+ " > ../data/myout/"+i+".myout";
	code1 = system(command)
	command = " diff data/out/"+i+".out data/myout/"+i+".myout"
	code2 = system(command)

	if code1 == 0 and code2 == 0:
		print("")
		print("-----------------------")
		print("    Passed Test: "+i)
		print("-----------------------")
		print("")
	else:
		print("")
		print("----------!!!----------")
		print("    Failed Test: "+i)
		print("----------!!!----------")
		print("")
		break
