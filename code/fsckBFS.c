#include <stdio.h>
#include <string.h>



#ifndef DISK_DRIVER_H
#include "disk_driver.h"
extern struct disk_operations disk_ops;
#endif

#ifndef FFS_SUPER_H
#include "ffs_super.h"
extern struct super_operations super_ops;
#endif

#ifndef FFS_INODE_H
#include "ffs_inode.h"
extern struct inode_operations inode_ops;
#endif

#ifndef FFS_BYTEMAP_H
#include "ffs_bytemap.h"
extern struct bytemap_operations bmap_ops;
#endif

void bmap_setup(struct super *sb, struct bytemap *in_bmap, struct bytemap *dt_bmap);

/**
 * A simplified file checker based on the UNIX fsck "File System Checker", adapted for the "Basic File System" developed for this project.
 * 
 * All errors called during the runtime of this program have been defined by the instructors of the course in the file 'ffs_errno.h'
 * 
 * @author Mauricio Polvora - 57971
 * @author Manuel Pereira - 57973
 */
int main(int argc, char *argv[]) {
    int ercode;
    unsigned int erflag = 0;

    struct super sb;
    struct bytemap in_bmap, dt_bmap;
    
    ercode = disk_ops.open(argv[1], 0);
    if(ercode < 0) return ercode;

    //Superblock check call (fills 'sb')
    ercode = super_ops.check(&sb, &erflag);
    if(ercode < 0) return ercode;

    bmap_setup(&sb, &in_bmap, &dt_bmap);

    //Inode check call. (fills the bytemap 'in_bmap' and 'dt_bmap')
    ercode = inode_ops.check(&sb, &in_bmap, &dt_bmap, &erflag);
    if(ercode < 0) return ercode;

    ercode = bmap_ops.check(&sb, &in_bmap, &dt_bmap, &erflag);
    if(ercode < 0) return ercode;

    printf("Detected %u errors\n", erflag);

    return 0;
}

/**
 * Auxiliary bytemap setup operations.
 * 
 * @in: [0] -> superblock structure, used for metadata scraping.
 *      [1] -> inode bytemap structure.
 *      [2] -> data bytemap structure.
 * 
 * @out: null
 */
void bmap_setup(struct super *sb, struct bytemap *in_bmap, struct bytemap *dt_bmap) {
    in_bmap->size = sb->ninodes;
    in_bmap->index = 0;
    //in_bmap->bmap = calloc(in_bmap->size, sizeof(unsigned int));
    memset(in_bmap->bmap, 0, sizeof(in_bmap->bmap));

    dt_bmap->size = sb->ndatablocks;
    dt_bmap->index = 0;
    //dt_bmap->bmap = calloc(dt_bmap->size, sizeof(unsigned int));
    memset(dt_bmap->bmap, 0, sizeof(dt_bmap->bmap));
}
