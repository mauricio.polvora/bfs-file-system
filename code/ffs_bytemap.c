#include <stdio.h>
#include <string.h>
#include <math.h>

#ifndef DISK_DRIVER_H
#include "disk_driver.h"
extern struct disk_operations disk_ops;
#endif

#ifndef FFS_SUPER_H
#include "ffs_super.h"
extern struct super_operations super_ops;
#endif

#ifndef FFS_BYTEMAP_H
#include "ffs_bytemap.h"
#endif

int compare_bmap(struct bytemap *bmap, struct bytemap *rd_bmap);


/* bytemap operations */
static int bytemap_read(struct bytemap *bmap, unsigned int max_entries, unsigned int absDskBlk) {
  int ercode;

  // using the absolute disk block value, read the bytemap block
  ercode = disk_ops.read(absDskBlk, bmap->bmap); 

  if (ercode < 0) return ercode;

  // use max_entries to set the bytemap size;  set index to 0
  bmap->size = max_entries;
  bmap->index = 0;

  return 0;
}

static int bytemap_getNextEntry(struct bytemap *bmap) {
  int entry;
  
  entry = bmap->bmap[bmap->index];

    bmap->index++;

  if(bmap->index == bmap->size) return -ENOSPC;
  
  return entry;
}


static int bytemap_setIndex(struct bytemap *bmap, unsigned int value) {

  bmap->index = value;

  return value;
}


void bytemap_printTable(struct bytemap *bmap) {

  int left = bmap->size, entry = 0;

  // prints 16 entries per line
  while (left) {
    if ((entry + 1) % 16) printf("%u ", bmap->bmap[entry]);
    else printf("%u\n", bmap->bmap[entry]);
    left--; entry++;
  }
  if ( entry%16 ) printf("\n"); // last NL for general case
}

int bytemap_check(struct super *sb, struct bytemap *in_bmap, struct bytemap *dt_bmap, unsigned int *erflag) {
  int ercode;
  struct bytemap rd_in_bmap, rd_dt_bmap;

  ercode = bytemap_read(&rd_in_bmap, DISK_BLOCK_SIZE, BMi_OFFSET);
  if(ercode < 0) return ercode;

  ercode = bytemap_read(&rd_dt_bmap, DISK_BLOCK_SIZE, sb->startDtBmap);
  if(ercode < 0) return ercode;

  ercode = compare_bmap(in_bmap, &rd_in_bmap);
  if(ercode != 0) {
    (*erflag)++;
    *in_bmap = rd_in_bmap;
  }

  ercode = compare_bmap(dt_bmap, &rd_dt_bmap);
  if(ercode != 0) {
    (*erflag)++;
    *dt_bmap = rd_dt_bmap;
  }

  return 0;
}

int compare_bmap(struct bytemap *bmap, struct bytemap *rd_bmap) {
  for(int i = 0; i < rd_bmap->size; i++) {
    if(bmap->bmap[i] != rd_bmap->bmap[i]) return i;
  }
  return 0;
}


struct bytemap_operations bmap_ops= {
	.read= bytemap_read,
	.getNextEntry= bytemap_getNextEntry,
	.setIndex= bytemap_setIndex,
	.printTable= bytemap_printTable,
  .check = bytemap_check
};
