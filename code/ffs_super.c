#include <stdio.h>
#include <string.h>
#include <math.h>

#ifndef DISK_DRIVER_H
#include "disk_driver.h"
extern struct disk_operations disk_ops;
#endif

#ifndef FFS_SUPER_H
#include "ffs_super.h"
#endif

#ifndef FFS_INODE_H
#include "ffs_inode.h"
#endif


static int super_read(struct super *sb) {
  int ercode;
  union sb_block sb_u;

  // Reads superblock data from disk to memory, stored in a union.
  ercode = disk_ops.read(SB_OFFSET, sb_u.data);

  if (ercode < 0) return ercode;

  // Copies data stored in the union to an external superblock. (given as a pointer)
  memcpy(sb, &sb_u.sb, sizeof(sb_u.sb));
  //*sb = sb_u.sb;

  return 0;
}

/* Helper functions */

void super_print(const struct super *sb) {
  printf("Superblock:\n");

  printf("%s\n", (sb->fsmagic == FS_MAGIC) ? "valid" : "invalid");

  printf("%u\n", sb->nblocks);
  printf("%u\n", sb->nbmapblocksinodes);
  printf("%u\n", sb->ninodeblocks);
  printf("%u\n", sb->ninodes);
  printf("%u\n", sb->nbmapblocksdata);
  printf("%u\n", sb->ndatablocks);
  printf("%u\n", sb->startInArea);
  printf("%u\n", sb->startDtBmap);
  printf("%u\n", sb->startDtArea);

  printf("%s\n", (sb->mounted == 1) ? "yes" : "no");
  }

void super_debug(const struct super *sb) {
  printf("Superblock:\n");

  printf("fsmagic \t\t 0x%05x\n", sb->fsmagic);
  printf("nblocks \t\t %u\n", sb->nblocks);
  printf("nbmapblocksinodes \t\t %u\n", sb->nbmapblocksinodes);
  printf("ninodeblocks \t\t %u\n", sb->ninodeblocks);
  printf("ninodes \t\t %u\n", sb->ninodes);
  printf("nbmapblocksdata \t\t %u\n", sb->nbmapblocksdata);
  printf("ndatablocks \t\t %u\n", sb->ndatablocks);
  printf("startInArea \t\t %u\n", sb->startInArea);
  printf("startDtBmap \t\t %u\n", sb->startDtBmap);
  printf("startDtArea \t\t %u\n", sb->startDtArea);
  printf("mounted \t\t %u\n", sb->mounted);
}

int super_check(struct super *sb, unsigned int *erflag) {
    int ercode;

    ercode = super_read(sb);
    if(ercode < 0) return ercode;

    if(sb->mounted == 1) {
        (*erflag)++;
        sb->mounted = 0;
    }

    if(sb->fsmagic != FS_MAGIC) {
        (*erflag)++;
        sb->fsmagic = FS_MAGIC;
    }

    //-------------------------------------------------------------------------
    //Size sanity check

    //Error code is now the total disk size, as reported by the disk_ops stat function.
    ercode = disk_ops.stat();
    if(ercode < 0) return ercode;

    if(sb->nblocks != (unsigned) ercode) {
        (*erflag)++;
        sb->nblocks = (unsigned) ercode;
    }

    //Error code is now the total *occupied* disk size, counted by the sum of all blocks.
    ercode = 1 + 1 + sb->nbmapblocksinodes + sb->nbmapblocksdata + sb->ninodeblocks + sb->ndatablocks; //The "+1" treat the superblock and directory block size, respectively.
    if(sb->nblocks != (unsigned) ercode) {
        (*erflag)++;
        sb->nblocks = (unsigned) ercode;
    }

    //-------------------------------------------------------------------------
    //Inode sanity check

    if(sb->ninodes > (sb->ninodeblocks * INODES_PER_BLOCK)) {
      (*erflag)++;
      sb->ninodeblocks = (sb->ninodes % INODES_PER_BLOCK) + 1;
    }

    //-------------------------------------------------------------------------
    //"Start" areas sanity check

    if(sb->startInArea != 2) {
      (*erflag)++;
      sb->startInArea = 2;
    }

    ercode = sb->startInArea + sb->ninodeblocks + 1;
    if(sb->startDtBmap != (unsigned) ercode) {
      (*erflag)++;
      sb->startDtBmap = (unsigned) ercode;
    }

    if(sb->startDtArea != (((unsigned) ercode) + 1)) {
      (*erflag)++;
      sb->startDtArea = (((unsigned) ercode) + 1);
    }

  return 0;
}

struct super_operations super_ops = {
	.read= &super_read,
	.print= &super_print,
	.debug= &super_debug,
  .check = &super_check
};
