#define FFS_INODE_H

#ifndef DISK_DRIVER_H
#include "disk_driver.h"
extern struct disk_operations disk_ops;
#endif


#ifndef FFS_SUPER_H 
#include "ffs_super.h"
extern struct super_operations super_ops;
#endif

#ifndef FFS_BYTEMAP_H
#include "ffs_bytemap.h"
extern struct bytemap_operations bmap_ops;
#endif



#define INODE_OFFSET		1 

// Note that inodes currently fit exactly in one block
#define INODES_PER_BLOCK	(DISK_BLOCK_SIZE / sizeof(struct inode))
#define POINTERS_PER_INODE	6

struct inode {
  unsigned int isvalid;
  unsigned int size;                        //in bytes
  unsigned int direct[POINTERS_PER_INODE];
};

union in_block {
  struct inode ino[INODES_PER_BLOCK];
  unsigned char data[DISK_BLOCK_SIZE];
};



/* operations on inode structures

  read: fills in an inode with its disk image
    parameters:
     @in: (absolute) inode number
     @out: pointer to inode structure
    errors:
     those resulting from disk operations

*/

/* Helper function prototypes */


struct inode_operations {
  void (*clear)(struct inode *in);
  int (*read)(unsigned int absDskBlk, unsigned int absinode, struct inode *in);
  int (*printTable)(unsigned int ninodeblocks, unsigned int ninodes, unsigned int absDskBlk);
  int (*printFileData)(unsigned int startInArea, unsigned int absinode, unsigned int startDtArea);
  int (*check)(struct super *sb, struct bytemap *in_bmap, struct bytemap *dt_bmap, unsigned int *erflag);
};
